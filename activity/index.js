// console.log('Hello World!');

/*
   - Create a function which is able to gather user details using prompt(). 
   - Create a function which is able to display simple data in the console.
   - Apply best practices in creating and defining functions by debugging erroneous code.

   Activity Output:
    1. prompt() and alert() used to show more interactivity in page.
	2.Values logged in console after invoking the function for objective  no.1

		Note: Name your own functions and variables but follow the conventions and best practice in naming functions and variables.
	3. Values shown in the console after invoking function created for objective 2.
	4. Values shown in the console after invoking function created for objective 3.
	5. Values shown in the console after invoking function debugged for objective 4.

	Activity Instruction:
	1. In the S17 folder, create an activity folder, an index.html file inside of it and link the index.js file.
	2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
	3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
	4.  Create a function which is able to prompt the user to provide their full name, age, and location. 
		- use prompt() and store the returned value into function scoped variables within the function. 
		- show an alert to thank the user for their input.
		- display the user's inputs in messages in the console.
 		- invoke the function to display the user’s information in the console.
		- follow the naming conventions for functions.
	5. Create a function which is able to print/display your top 5 favorite bands/musical artists. 
		- invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	6. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating. 
		- look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
        - invoke the function to display your information in the console.
		- follow the naming conventions for functions.
	7. Debugging Practice - Debug the given codes and functions to avoid errors.
		- check the variable names.
		- check the variable scope.
		- check function invocation/declaration.
		- comment out unusable codes.
	8. Create a git repository named S17.
 	9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	10. Add the link in Boodle.

	How you will be evaluated
	1. No errors should be logged in the console.
	2. prompt() is used to gather information.
	3. alert() is used to show information.
	4. All values must be properly logged in the console.
	5. All variables are named appropriately and defines the value it contains.
	6. All functions are named appropriately and follows naming conventions.

 */
let userDetails = function(){
    let userFullName = prompt('What is your Full Name?');
    let userAge = prompt('How old are you?');
    let userLocation = prompt('Where are you from?');
    console.log('Hello, ' + userFullName + '.');
    console.log('You are ' + userAge + ' years old.');
    console.log('You live in ' + userLocation + '.');
    alert('Thank you for your input!');
};
userDetails();

let displayFavArtists = function(){
    let artist1 = '1. Drake'; 
    let artist2 = '2. 50 Cent'; 
    let artist3 = '3. Eminem'; 
    let artist4 = '4. Tupac'; 
    let artist5 = '5. Dr. Dre';
    console.log('My Top 5 Artists Are:')
    console.log(artist1);
    console.log(artist2);
    console.log(artist3);
    console.log(artist4);
    console.log(artist5);
};
displayFavArtists();

let displayFavMovies = function(){
    let movie1 = '1. The Pursuit of Happyness'; 
    let rating1 = 'Rotten Tomatoes Rating: 67%'
    let movie2 = '2. Parasite';
    let rating2 = 'Rotten Tomatoes Rating: 99%'
    let movie3 = '3. Avengers: End Game'; 
    let rating3 = 'Rotten Tomatoes Rating: 94%'
    let movie4 = '4. Logan'; 
    let rating4 = 'Rotten Tomatoes Rating: 94%'
    let movie5 = '5. Saving Private Ryan';
    let rating5 = 'Rotten Tomatoes Rating: 93%'
    
    console.log('My Top 5 Movies Are:')
    console.log(movie1);
    console.log(rating1);
    console.log(movie2);
    console.log(rating2);
    console.log(movie3);
    console.log(rating3);
    console.log(movie4);
    console.log(rating4);
    console.log(movie5);
    console.log(rating5);
};
displayFavMovies();


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1); commented out since this is not global scope
// console.log(friend2); commented out since this is not global scope